﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TwoStroke_DiyManager : MonoBehaviour {

	public static TwoStroke_DiyManager instance;

	public GameObject searchPanel, uiPanels;

	public GameObject sparkPlugExplosion, combustionSmoke, combustionFuel;
	// Use this for initialization
	void Awake () {
		instance = this;
		VuforiaUnity.SetHint(VuforiaUnity.VuforiaHint.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 2);
	}
		
	public void CameraClick(){
		StartCoroutine (EnableCameraUI());
	}

	IEnumerator EnableCameraUI(){
		uiPanels.SetActive (false);
		yield return new WaitForSeconds (3.5f);
		uiPanels.SetActive (true);
	}

}
