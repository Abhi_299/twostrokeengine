﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStroke_ObjectManager : MonoBehaviour {

	public GameObject labels;
	public Animator engine_Animator;
	public MeshRenderer combutionPart,crankCasePart;
	public List<MeshRenderer> lightPart,darkPart;
	public GameObject sparkPlugExplosion, exhaustSmoke;
	public Animator combustionSmoke, combustionFuel;
	public GameObject crankCaseFuel, fuelTank;
	public GameObject straightArrow, intakePart, powerPart, compressPart, exhaustPart;
	public GameObject pistonLabel, compressLabel, intakeExplosion;


	public void WorkingActivityEngine(){
		for (int i = 0; i < lightPart.Count; i++) {
			lightPart [i].material = TwoStroke_UiController.instance.transparentMat;
		}
		for (int i = 0; i < darkPart.Count; i++) {
			darkPart [i].material = TwoStroke_UiController.instance.transparentMat;
		}
		combutionPart.material = TwoStroke_UiController.instance.transparentMat;
		crankCasePart.material = TwoStroke_UiController.instance.transparentMat;
	}

	public void NormalEngine(){
		for (int i = 0; i < lightPart.Count; i++) {
			lightPart [i].material = TwoStroke_UiController.instance.engineLightMat;
		}
		for (int i = 0; i < darkPart.Count; i++) {
			darkPart [i].material = TwoStroke_UiController.instance.engineDarkMat;
		}
		combutionPart.material = TwoStroke_UiController.instance.engineLightMat;
		crankCasePart.material = TwoStroke_UiController.instance.engineDarkMat;
	}
}
