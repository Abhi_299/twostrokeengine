﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwoStroke_WorkingActivity : MonoBehaviour {

	public static TwoStroke_WorkingActivity instance;

	public Slider rpmController;
	public Image rpmScale;
	public Animator powerValve;

	public Image intakeBtn, compressionBtn, powerBtn, exhaustBtn, speedRangeIcon;
	public Sprite normalBtnSprite, highlightBtnSprite;
	public GameObject instSpeedRange;

	bool outRange, speedRangeIncrease;
	float rpmSliderValue, speedRangeAlpha ;

	void Awake(){
		instance = this;
	}

	void FixedUpdate(){
		if (!outRange) {
			return;
		}

		if (speedRangeAlpha >= 1 && speedRangeIncrease) {
			speedRangeIncrease = false;
		}else if (speedRangeAlpha <= 0 && !speedRangeIncrease){
			speedRangeIncrease = true;
		}

		if (speedRangeIncrease) {
			speedRangeAlpha+=0.1f;
			Debug.Log ("Increases- "+speedRangeAlpha);
		} else {
			speedRangeAlpha-=0.1f;
			Debug.Log ("Decreases- "+speedRangeAlpha);
		}

		speedRangeIcon.canvasRenderer.SetAlpha(speedRangeAlpha);
	}

	public void OnRpm_Controller() {

		if (rpmController.value > 8.8f && rpmController.value - 0.01f > rpmSliderValue) {
			speedRangeIcon.color = new Color (203, 0, 0, 255);
			instSpeedRange.SetActive (true);
			outRange = true;
		} else if(rpmController.value <= 8.8f ){
			speedRangeIcon.color = new Color (0, 203, 0, 255);
			instSpeedRange.SetActive (false);
			outRange = false;
			speedRangeIcon.canvasRenderer.SetAlpha(1);
		}

		if (rpmController.value > 8.8f) {
			TwoStroke_UiController.instance.engineWarningSound.gameObject.SetActive (true);
		} else {
			TwoStroke_UiController.instance.engineWarningSound.gameObject.SetActive (false);
		}

		rpmSliderValue = rpmController.value;
		rpmScale.fillAmount = rpmSliderValue * 0.1f;

		TwoStroke_UiController.instance.engineSound.gameObject.SetActive (true);
		TwoStroke_UiController.instance.ManageEngineSound (rpmSliderValue);

		powerValve.SetFloat ("OpenValve",rpmSliderValue * 0.09f);
		if (TwoStroke_UiController.instance.currObjectScript != null) {
			TwoStroke_UiController.instance.currObjectScript.engine_Animator.speed = rpmSliderValue * 0.1f;
			TwoStroke_UiController.instance.currObjectScript.combustionSmoke.speed = rpmSliderValue * 0.2f;
			TwoStroke_UiController.instance.currObjectScript.combustionFuel.speed = rpmSliderValue * 0.2f;
//			if (rpmController.value > 2) {
				TwoStroke_UiController.instance.currObjectScript.exhaustSmoke.SetActive (true);
//			}
		}

		RemoveHighlight ();
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Intake",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Exhaust",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Power",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Compress",false);

		TwoStroke_UiController.instance.currObjectScript.sparkPlugExplosion.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.combustionSmoke.gameObject.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.combustionFuel.gameObject.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.crankCaseFuel.gameObject.SetActive (true);

		DisableAllPart();
		TwoStroke_UiController.instance.currObjectScript.straightArrow.SetActive (false);

		TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (true);
	}

	public void Intake(){
		StartCoroutine (DisableExplosion());
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Intake",true);
		TwoStroke_UiController.instance.engineSound.gameObject.SetActive (false);
		DisableAllPart ();
		RemoveHighlight ();
		intakeBtn.sprite = highlightBtnSprite;
		TwoStroke_UiController.instance.currObjectScript.straightArrow.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.pistonLabel.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.intakePart.SetActive (true);
//		if (isIntake) {
//			
//			TwoStroke_UiController.instance.currObjectScript.compressLabel.SetActive (false);
//
//
//		} else {
//			TwoStroke_UiController.instance.currObjectScript.intakePart.SetActive (false);
//			compressionBtn.sprite = highlightBtnSprite;
//			TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (false);
//
//		}
		TwoStroke_UiController.instance.currObjectScript.straightArrow.transform.localEulerAngles = new Vector3 (-90, 0, 0);
	}

	public void Compression() {
		StartCoroutine (DisableExplosion());
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Compress",true);
		TwoStroke_UiController.instance.engineSound.gameObject.SetActive (false);
		DisableAllPart ();
		RemoveHighlight ();
		compressionBtn.sprite = highlightBtnSprite;
		TwoStroke_UiController.instance.currObjectScript.straightArrow.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.compressPart.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.pistonLabel.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.straightArrow.transform.localEulerAngles = new Vector3 (-90, 0, 0);
		TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (false);
	}

	public void Exhaust(){
		StartCoroutine (DisableExplosion());
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Exhaust",true);
		TwoStroke_UiController.instance.engineSound.gameObject.SetActive (false);
		DisableAllPart ();
		RemoveHighlight ();
		exhaustBtn.sprite = highlightBtnSprite;
		TwoStroke_UiController.instance.currObjectScript.pistonLabel.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.straightArrow.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.exhaustPart.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.straightArrow.transform.localEulerAngles = new Vector3 (90, 0, 0);
		TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.exhaustSmoke.SetActive (true);

	}

	public void Power(){
		StartCoroutine (DisableExplosion());
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Power",true);
		TwoStroke_UiController.instance.engineSound.gameObject.SetActive (false);
		DisableAllPart ();
		RemoveHighlight ();
		powerBtn.sprite = highlightBtnSprite;
		TwoStroke_UiController.instance.currObjectScript.pistonLabel.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.straightArrow.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.powerPart.SetActive (true);
		TwoStroke_UiController.instance.currObjectScript.straightArrow.transform.localEulerAngles = new Vector3 (90, 0, 0);
		TwoStroke_UiController.instance.currObjectScript.intakeExplosion.SetActive (false);

	}

	IEnumerator DisableExplosion(){
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.speed = 1;
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Intake",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Exhaust",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Power",false);
		TwoStroke_UiController.instance.currObjectScript.engine_Animator.SetBool ("Compress",false);
		TwoStroke_UiController.instance.currObjectScript.exhaustSmoke.SetActive (false);

		yield return new WaitForSeconds (0.5f);
		TwoStroke_UiController.instance.currObjectScript.sparkPlugExplosion.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.combustionSmoke.gameObject.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.combustionFuel.gameObject.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.crankCaseFuel.gameObject.SetActive (false);
	}

	void DisableAllPart(){
		
		TwoStroke_UiController.instance.currObjectScript.powerPart.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.compressPart.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.intakePart.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.exhaustPart.SetActive (false);
		TwoStroke_UiController.instance.currObjectScript.pistonLabel.SetActive (false);

		TwoStroke_UiController.instance.currObjectScript.compressLabel.SetActive (true);
	}

	void RemoveHighlight() {
		intakeBtn.sprite = normalBtnSprite;
		compressionBtn.sprite = normalBtnSprite;
		powerBtn.sprite = normalBtnSprite;
		exhaustBtn.sprite = normalBtnSprite;
	}
}
