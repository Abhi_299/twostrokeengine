﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class TwoStroke_UiController : MonoBehaviour {

	public static TwoStroke_UiController instance;

	[Header("Ui Panels and Buttons")]
	public GameObject activityBtns;
	public GameObject instructionPanel, workingActivityPanel;
	public GameObject homeBtn,diyBtn,startInstpanel,searchPanel;
	public GameObject uiPanels;
	public AudioSource engineSound,engineWarningSound;

	[Header("Panel Text")]
	public Text instructionPanelTxt;
	public Text instructionlabelTxt;

	[Header("Materials")]
	public Material transparentMat;
	public Material engineDarkMat,engineLightMat;

//	[Header("Animation clip")]
//	public AnimationClip engineMergeAnim;

	internal GameObject currObject;
	internal bool isLabelInst;
	internal bool isModelDetected;
	internal TwoStroke_ObjectManager currObjectScript;

	// Use this for initialization
	void Awake () {
		instance = this;
		VuforiaUnity.SetHint(VuforiaUnity.VuforiaHint.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 1);
	}

	void OnEnable() {
		LoadModelOnDetection.isModelLoaded += Model_Loading;
	}

	void OnDisable() {
		LoadModelOnDetection.isModelLoaded -= Model_Loading;
	}

	#region Delegate Handler
	public void Model_Loading (string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType){
		isModelDetected = status;
		StartCoroutine (OnModel_Loading(_transform,status));
	}

	IEnumerator OnModel_Loading(Transform _transform, bool status) {

		if (!status) {
			DisableUiPanels ();
		}

		yield return new WaitForEndOfFrame ();

		if (status) {
			searchPanel.SetActive (false);
			currObject = _transform.GetChild (0).gameObject;
			currObjectScript = currObject.GetComponent<TwoStroke_ObjectManager> ();
			currObjectScript.engine_Animator.speed = 1;
			activityBtns.SetActive (true);
			TwoStroke_WorkingActivity.instance.rpmController.value = 1;
			TwoStroke_WorkingActivity.instance.OnRpm_Controller ();
			currObjectScript.engine_Animator.speed = 1;
			currObjectScript.combustionSmoke.gameObject.SetActive (false);
			currObjectScript.combustionFuel.gameObject.SetActive (false);

			TwoStroke_ZoomInOut.instance.currModel = currObject;
			TwoStroke_ZoomInOut.instance.isModlDetected = true;
			engineSound.gameObject.SetActive (false);
		}
	}

	#endregion

	void DisableUiPanels(){
		TwoStroke_ZoomInOut.instance.isModlDetected = false;
		engineSound.gameObject.SetActive (false);

		searchPanel.SetActive (true);
		activityBtns.SetActive (false);
		instructionPanel.SetActive (false);
		workingActivityPanel.SetActive (false);
		startInstpanel.SetActive (false);
		if (currObjectScript != null) {
			currObjectScript.labels.SetActive (false);
		}
		isLabelInst = false;
		diyBtn.SetActive (true);

		engineWarningSound.gameObject.SetActive (false);
	}

	public void Click_InfoActivityBtn() {
		activityBtns.SetActive (false);
		homeBtn.SetActive (true);
		currObjectScript.labels.SetActive (true);
		currObjectScript.engine_Animator.SetBool ("WorkEngine",false);

		currObjectScript.sparkPlugExplosion.SetActive (false);
		currObjectScript.combustionSmoke.gameObject.SetActive (false);
		currObjectScript.combustionFuel.gameObject.SetActive (false);
		currObjectScript.crankCaseFuel.SetActive (false);
		currObjectScript.fuelTank.SetActive (false);

		currObjectScript.combutionPart.material = transparentMat;
		currObjectScript.crankCasePart.material = transparentMat;
		diyBtn.SetActive (false);
		currObjectScript.intakeExplosion.SetActive (false);
	}

	public void Click_homeBtn() {

		if (isLabelInst) {
			
			isLabelInst = false;
			TwoStroke_LabelManager currlabelManager = currObject.GetComponent<TwoStroke_LabelManager> ();
			for (int i = 0; i < currlabelManager.labelObject.Count; i++) {
				currlabelManager.labelObject [i].labelObject.transform.localScale = Vector3.zero;
			}
			currlabelManager.engineInnerPart.SetActive (true);
			currObjectScript.labels.SetActive (true);
			instructionPanel.SetActive (false);
		} else {
		
			activityBtns.SetActive (true);
			instructionPanel.SetActive (false);
			workingActivityPanel.SetActive (false);
			currObjectScript.labels.SetActive (false);
			homeBtn.SetActive (false);
			currObjectScript.engine_Animator.speed = 1;

			currObjectScript.engine_Animator.SetBool ("WorkEngine",false);
			currObjectScript.labels.SetActive (false);
			currObjectScript.combutionPart.material = engineLightMat;
			currObjectScript.crankCasePart.material = engineDarkMat;
			currObjectScript.NormalEngine ();

			TwoStroke_WorkingActivity.instance.rpmController.value = 1;
			TwoStroke_WorkingActivity.instance.OnRpm_Controller ();
			currObjectScript.engine_Animator.speed = 1;
			currObjectScript.combustionSmoke.gameObject.SetActive (false);
			currObjectScript.combustionFuel.gameObject.SetActive (false);
			currObjectScript.exhaustSmoke.SetActive (false);
			diyBtn.SetActive (true);
			currObjectScript.intakeExplosion.SetActive (false);

			engineSound.gameObject.SetActive (false);
			engineWarningSound.gameObject.SetActive (false);
		}

	}

	public void Click_WorkingActivity(){
		diyBtn.SetActive (false);
		workingActivityPanel.SetActive (true);
		homeBtn.SetActive (true);
		activityBtns.SetActive (false);

		currObjectScript.engine_Animator.SetBool ("Intake",false);
		currObjectScript.engine_Animator.SetBool ("Exhaust",false);
		currObjectScript.engine_Animator.SetBool ("Power",false);
		currObjectScript.engine_Animator.SetBool ("Compress",false);

		currObjectScript.sparkPlugExplosion.SetActive (true);
		currObjectScript.combustionSmoke.gameObject.SetActive (true);
		currObjectScript.combustionFuel.gameObject.SetActive (true);
		currObjectScript.crankCaseFuel.SetActive (true);
		currObjectScript.fuelTank.SetActive (true);

		currObjectScript.engine_Animator.SetBool ("WorkEngine",true);
		currObjectScript.engine_Animator.speed = 0.1f;
		currObjectScript.combustionSmoke.speed = 0.2f;
		currObjectScript.combustionFuel.speed = 0.2f;
		currObjectScript.exhaustSmoke.SetActive (false);
		currObjectScript.WorkingActivityEngine ();

		engineSound.gameObject.SetActive (true);
		ManageEngineSound (0.1f);
	}

	public void Camera_Click(){
		StartCoroutine (ManageUiPanels());
	}

	IEnumerator ManageUiPanels(){
		uiPanels.SetActive (false);
		yield return new WaitForSeconds (3.5f);
		uiPanels.SetActive (true);
	}

	public void ManageEngineSound(float engineSpeed){
		if (!engineSound.isPlaying) {
			engineSound.Play ();
		}
		engineSound.pitch = engineSpeed/14.2f+0.3f;
	}
}
