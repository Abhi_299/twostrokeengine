﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ImageTargetInfo
{
	public string cardName;
	public GameObject imageTarget;
	public GameObject selectObject;
}


[System.Serializable]
public class DamPartsTransform
{
	public string partsName;
	public Transform partsTransform;
}


public class ImageAlphaHandler : MonoBehaviour
{

	#region public varaibles.
	public string emptyobjectName = "AssemblePartsParent";
	public const int maxCardDetection = 2;
	public GameObject scrollViewParent, attachButton, alertPanel, congratsPanel;
	public GameObject mergeEngine;
	[SerializeField] List<DamPartsTransform> damPartsTransforms = new List<DamPartsTransform>();
	public List<ImageTargetInfo> detectedObjectList = new List<ImageTargetInfo>();
	[SerializeField] List<string> attachedObject = new List<string>();
	public static ImageAlphaHandler instance;
	#endregion


	#region private variables.
	GameObject emptyObject = null;
	static int detectedObject;
	string currentObjectName;
    #endregion

    private void Start()
	{
		instance = this;
		UIOnOffButton(congratsPanel, false);
		UIOnOffButton(alertPanel, false);
	}

	// Use this for initialization
	void OnEnable()
	{

		if (scrollViewParent.transform.childCount > 0)
		{
			for (int i = 0; i < scrollViewParent.transform.childCount; i++)
			{
				scrollViewParent.transform.GetChild(i).GetComponent<Image>().canvasRenderer.SetAlpha(0.3f);
			}
		}

		LoadModelOnDetection.isModelLoaded += HandleOnModelLoaded;
		TrackableEventHandler.onImageTargetDetected += OnModelLost;
	}

	private void OnDisable()
	{
		LoadModelOnDetection.isModelLoaded -= HandleOnModelLoaded;
		TrackableEventHandler.onImageTargetDetected -= OnModelLost;
	}


	void OnModelLost(string name, Transform _transform, bool status){
		if (!status) {
			currentObjectName = null;
			if (detectedObject > 0)
			{
				detectedObject--;
			}
			if (attachButton.activeInHierarchy)
				UIOnOffButton(attachButton, false);


			ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == name.ToLower());
			if (imageTargetInfo != null)
			{
				detectedObjectList.Remove(imageTargetInfo);
			}

			if (detectedObjectList.Count == 0) {
				
				TwoStroke_ZoomInOut.instance.enabled = false;
				TwoStroke_ZoomInOut.instance.isModlDetected = false;
				TwoStroke_DiyManager.instance.searchPanel.SetActive (true);

				mergeEngine.transform.SetParent (null);
				mergeEngine.SetActive (false);

				Destroy (emptyObject);
				emptyObject = null;
				attachedObject.Clear ();

				if (scrollViewParent.transform.childCount > 0) {
					for (int i = 0; i < scrollViewParent.transform.childCount; i++) {
						string fadeOutCard = attachedObject.Find (item => item.ToLower () == scrollViewParent.transform.GetChild (i).name.ToLower ());
						if (string.IsNullOrEmpty (fadeOutCard))
							scrollViewParent.transform.GetChild (i).GetComponent<Image> ().canvasRenderer.SetAlpha (.3f);
					}
				}
			} else if (detectedObjectList.Count > 0 && attachedObject.Count > 0 && attachedObject.Contains (detectedObjectList [0].cardName)) {

				if (emptyObject != null) {
					emptyObject.transform.SetParent (detectedObjectList [0].imageTarget.transform);
					emptyObject.transform.localPosition = Vector3.zero;
					emptyObject.transform.localEulerAngles = new Vector3 (0, -90, 0);
					emptyObject.transform.localScale = Vector3.one * 0.5f;
					Renderer[] renderers = emptyObject.GetComponentsInChildren<Renderer> ();
					Debug.Log ("Parent changed");
					for (int i = 0; i < renderers.Length; i++) {
						renderers [i].enabled = true;
					}
				}
			} else { //Abhishek

				TwoStroke_ZoomInOut.instance.enabled = false;
				TwoStroke_ZoomInOut.instance.isModlDetected = false;

				mergeEngine.transform.SetParent (null);
				mergeEngine.SetActive (false);

				Destroy (emptyObject);
				emptyObject = null;
				attachedObject.Clear ();

				if (scrollViewParent.transform.childCount > 0) {
					for (int i = 0; i < scrollViewParent.transform.childCount; i++) {
						string fadeOutCard = attachedObject.Find (item => item.ToLower () == scrollViewParent.transform.GetChild (i).name.ToLower ());
						if (string.IsNullOrEmpty (fadeOutCard))
							scrollViewParent.transform.GetChild (i).GetComponent<Image> ().canvasRenderer.SetAlpha (.3f);
					}
				}

				if (scrollViewParent.transform.childCount > 0)
				{
					for (int i = 0; i < scrollViewParent.transform.childCount; i++)
					{
						if (detectedObjectList [0].cardName == scrollViewParent.transform.GetChild(i).name.ToLower())
							scrollViewParent.transform.GetChild(i).GetComponent<Image>().canvasRenderer.SetAlpha(1f);
					}
				}

			}


			if (scrollViewParent.transform.childCount > 0)
			{
				for (int i = 0; i < scrollViewParent.transform.childCount; i++)
				{
					string fadeOutCard = attachedObject.Find(item => item.ToLower() == scrollViewParent.transform.GetChild(i).name.ToLower());
					if (string.IsNullOrEmpty(fadeOutCard) && name.ToLower() == scrollViewParent.transform.GetChild(i).name.ToLower())
						scrollViewParent.transform.GetChild(i).GetComponent<Image>().canvasRenderer.SetAlpha(.3f);
				}
			}
		}
	}

	private void HandleOnModelLoaded(string cardName, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
	{
		if (status)
		{
			detectedObject++;
			currentObjectName = cardName;
			TwoStroke_DiyManager.instance.searchPanel.SetActive(false);
            ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == cardName.ToLower());
			if (imageTargetInfo == null)
			{
				imageTargetInfo = new ImageTargetInfo();
				imageTargetInfo.cardName = cardName;
				imageTargetInfo.imageTarget = _transform.gameObject;
				imageTargetInfo.selectObject = _transform.GetChild(0).gameObject;
				detectedObjectList.Add(imageTargetInfo);
				imageTargetInfo = null;
			}



			string currentObject = attachedObject.Find(item => item.ToLower() == cardName.ToLower());
			if (currentObject != null)
			{
				Destroy(_transform.GetChild(0).gameObject);
			}
			if (detectedObjectList.Count == 2 && currentObject == null)
			{
				//Attach buttonshow;
				UIOnOffButton(attachButton, true);
			}
			else
			{
				UIOnOffButton(attachButton, false);
			}


			if (scrollViewParent.transform.childCount > 0)
			{
				for (int i = 0; i < scrollViewParent.transform.childCount; i++)
				{
					if (cardName.ToLower() == scrollViewParent.transform.GetChild(i).name.ToLower())
						scrollViewParent.transform.GetChild(i).GetComponent<Image>().canvasRenderer.SetAlpha(1f);
				}
			}

		}


	}

	void UIOnOffButton(GameObject selectedObject, bool status)
	{
		if (selectedObject != null)
		{
			selectedObject.SetActive(status);
		}
	}

	bool isModelAttached()
	{
		if (detectedObjectList.Count == maxCardDetection)
		{

			int[] detectStoreCard = new int[detectedObjectList.Count];
			int detectStoreIndex = -1;
			for (int i = 0; i < scrollViewParent.transform.childCount; i++)
			{
				int cardIndex = detectedObjectList.FindIndex(item => item.cardName.ToLower() == scrollViewParent.transform.GetChild(i).name.ToLower());
				if (cardIndex > -1)
				{
					detectStoreIndex++;
					detectStoreCard[detectStoreIndex] = i;
				}
			}
			if (detectStoreCard.Length == 2)
			{
				int result = detectStoreCard[1] - detectStoreCard[0];

				if (Math.Abs(result) == 1)
				{

					return true;
				}
				else if (attachedObject.Count > 0)
				{
					int detectedIndex = -1;

					for (int a = 0; a < scrollViewParent.transform.childCount; a++)
					{
						if (scrollViewParent.transform.GetChild(a).name.ToLower() == currentObjectName.ToLower())
						{
							detectedIndex = a;
						}
					}

					for (int i = 0; i < scrollViewParent.transform.childCount; i++)
					{
						int cardIndex = attachedObject.FindIndex(item => item.ToLower() == scrollViewParent.transform.GetChild(i).name.ToLower());
						if (cardIndex > -1)
						{
							if ((Math.Abs(detectedIndex - i) == 1))
								return true;
						}
					}
				}
				else
				{
					return false;
				}

			}


		}
		return false;
	}


	public void ModelAttach()
	{
		if (detectedObjectList.Count == maxCardDetection)
		{
			if (isModelAttached())
			{

				if (emptyObject == null)
				{
					emptyObject = new GameObject();
                    emptyObject.name = emptyobjectName;
					emptyObject.transform.SetParent(detectedObjectList[0].imageTarget.transform);
					emptyObject.transform.localPosition = Vector3.zero;
					emptyObject.transform.localEulerAngles = new Vector3 (0, -90, 0);
					emptyObject.transform.localScale = Vector3.one*0.5f;
					detectedObjectList[0].selectObject.transform.SetParent(emptyObject.transform);
//					detectedObjectList[0].selectObject.transform.localPosition = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[0].cardName.ToLower()).partsTransform.localPosition;
					detectedObjectList[0].selectObject.transform.localRotation = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[0].cardName.ToLower()).partsTransform.localRotation;
//					detectedObjectList[0].selectObject.transform.localScale = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[0].cardName.ToLower()).partsTransform.localScale;
					StartCoroutine(MoveObject(detectedObjectList[0]));
				}

				for (int i = 1; i < detectedObjectList.Count; i++)
				{
					detectedObjectList[i].selectObject.transform.SetParent(emptyObject.transform);
//					detectedObjectList[i].selectObject.transform.localPosition = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[i].cardName.ToLower()).partsTransform.localPosition;
					detectedObjectList[i].selectObject.transform.localRotation = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[i].cardName.ToLower()).partsTransform.localRotation;
//					detectedObjectList[i].selectObject.transform.localScale = damPartsTransforms.Find(item => item.partsName.ToLower() == detectedObjectList[i].cardName.ToLower()).partsTransform.localScale;
					StartCoroutine(MoveObject(detectedObjectList[i]));
				}


				// attached the model.
				for (int j = 0; j < detectedObjectList.Count; j++)
				{
					string currentObject = attachedObject.Find(item => item.ToLower() == detectedObjectList[j].cardName.ToLower());
					if (currentObject == null)
					{
						attachedObject.Add(detectedObjectList[j].cardName);
					}
				}
				UIOnOffButton(attachButton, false);
				if (attachedObject.Count == scrollViewParent.transform.childCount)
				{
					for (int i = 0; i < emptyObject.transform.childCount; i++) {
						emptyObject.transform.GetChild (i).gameObject.SetActive(false);
					}
					mergeEngine.transform.SetParent(emptyObject.transform);
					mergeEngine.SetActive (true);
					mergeEngine.transform.localPosition = Vector3.zero;
					mergeEngine.transform.localEulerAngles = Vector3.zero;
					mergeEngine.transform.localScale = Vector3.one*3;

					MeshRenderer[] mergeMesh = mergeEngine.GetComponentsInChildren<MeshRenderer> ();
					for (int i = 0; i < mergeMesh.Length; i++) {
						mergeMesh [i].enabled = true;
					}

					UIOnOffButton(congratsPanel, true);

					if (TwoStroke_ZoomInOut.instance != null) {
						TwoStroke_ZoomInOut.instance.currModel = emptyObject;
                    }
					TwoStroke_ZoomInOut.instance.isModlDetected = true;
					TwoStroke_ZoomInOut.instance.enabled = true;
                }

			}
			else
			{
				// show Popup place the correct marker.
				UIOnOffButton(alertPanel, true);
			}
		}
	}


	IEnumerator MoveObject(ImageTargetInfo _currTargetInfo) {
		Vector3 _pos= damPartsTransforms.Find(item => item.partsName.ToLower() == _currTargetInfo.cardName.ToLower()).partsTransform.localPosition;
		Vector3 _scale = damPartsTransforms.Find(item => item.partsName.ToLower() == _currTargetInfo.cardName.ToLower()).partsTransform.localScale;
		float _Time = 0;

		while (_Time < 1)
		{
			if (detectedObjectList.Count > 0)
			{
				_currTargetInfo.selectObject.transform.localScale = Vector3.Lerp(_currTargetInfo.selectObject.transform.localScale,_scale,_Time);
				_currTargetInfo.selectObject.transform.localPosition = Vector3.Lerp(_currTargetInfo.selectObject.transform.localPosition,_pos,_Time);
				yield return new WaitForSeconds(0.01f);
			}
			else
			{
				yield break;
			}
			_Time += 0.01f;
		}

	}


}
