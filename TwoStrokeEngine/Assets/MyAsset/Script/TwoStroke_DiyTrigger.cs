﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStroke_DiyTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider col){

		if (col.gameObject.name == "CombutionUpperCollider") {
			TwoStroke_DiyManager.instance.sparkPlugExplosion.SetActive (true);
			TwoStroke_DiyManager.instance.combustionSmoke.gameObject.SetActive (false);
			TwoStroke_DiyManager.instance.combustionSmoke.gameObject.SetActive (true);
			TwoStroke_DiyManager.instance.combustionFuel.gameObject.SetActive (false);
		} 
		else if (col.gameObject.name == "CombutionFuelCollider") {
//			TwoStroke_DiyManager.instance.exhaustSmoke.SetActive (true);
			TwoStroke_DiyManager.instance.combustionFuel.gameObject.SetActive (true);
		}
	}

	void OnTriggerExit(Collider col){

		if ( col.gameObject.name=="CombutionDownCollider") {
			TwoStroke_DiyManager.instance.sparkPlugExplosion.SetActive (false);
		} 
//		else if (col.gameObject.name == "CombutionFuelCollider") {
//			TwoStroke_DiyManager.instance.exhaustSmoke.SetActive (false);
//		}

	}

}
