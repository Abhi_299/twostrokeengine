﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStroke_PistonTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (TwoStroke_UiController.instance.currObjectScript != null) {
			if (col.gameObject.name == "CombutionUpperCollider") {
				TwoStroke_UiController.instance.currObjectScript.sparkPlugExplosion.SetActive (true);
				TwoStroke_UiController.instance.currObjectScript.combustionSmoke.gameObject.SetActive (false);
				TwoStroke_UiController.instance.currObjectScript.combustionSmoke.gameObject.SetActive (true);
				TwoStroke_UiController.instance.currObjectScript.combustionFuel.gameObject.SetActive (false);
			} else if (col.gameObject.name == "CombutionFuelCollider") {
				if (TwoStroke_WorkingActivity.instance.rpmController.value < 2) {
					TwoStroke_UiController.instance.currObjectScript.exhaustSmoke.SetActive (true);
				}
				TwoStroke_UiController.instance.currObjectScript.combustionFuel.gameObject.SetActive (true);
				Debug.Log ("Fuel activated");
			}
		}
	}

	void OnTriggerExit(Collider col){
		if (TwoStroke_UiController.instance.currObjectScript != null) {
			if ( col.gameObject.name=="CombutionDownCollider") {
				TwoStroke_UiController.instance.currObjectScript.sparkPlugExplosion.SetActive (false);
			} else if (col.gameObject.name == "CombutionFuelCollider") {
				if (TwoStroke_WorkingActivity.instance.rpmController.value < 2) {
					TwoStroke_UiController.instance.currObjectScript.exhaustSmoke.SetActive (false);
				}
			}
		}

	}
}
