﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStroke_labelActivity : MonoBehaviour {

	TwoStroke_LabelManager currLabelManager;

	void OnEnable(){
		InputTouchHit.OnScreenTouch += OnLabelClick;
	}

	void OnDisable(){
		InputTouchHit.OnScreenTouch -= OnLabelClick;
	}

	public void OnLabelClick(RaycastHit hit){
		if (TwoStroke_UiController.instance.currObject != null) {
			currLabelManager = TwoStroke_UiController.instance.currObject.GetComponent<TwoStroke_LabelManager> ();

			for (int i = 0; i < currLabelManager.labelObject.Count; i++) {
				if (hit.transform.parent.name == currLabelManager.labelObject [i].labelPanel.name) {
					currLabelManager.engineInnerPart.SetActive (false);
					StartCoroutine (MoveEnginePart(currLabelManager.labelObject [i].labelObject.transform));
					TwoStroke_UiController.instance.currObject.GetComponent<TwoStroke_ObjectManager> ().labels.SetActive (false);
					TwoStroke_UiController.instance.instructionlabelTxt.text = currLabelManager.labelObject [i].labelName;
					TwoStroke_UiController.instance.instructionPanelTxt.text = currLabelManager.labelObject [i].labelInstruction;
					TwoStroke_UiController.instance.instructionPanel.SetActive (true);
					TwoStroke_UiController.instance.isLabelInst = true;
					TwoStroke_UiController.instance.homeBtn.SetActive (false);
					break;
				}
			}
		}
	}


	IEnumerator MoveEnginePart(Transform _transform){
		float moveTime = 0.01f;
		while (moveTime < 1) {
			yield return new WaitForSeconds (0.01f);
			if (!TwoStroke_UiController.instance.isModelDetected) {
				yield break;
			}
			_transform.localScale = Vector3.Lerp (Vector3.zero,Vector3.one,moveTime);
			moveTime += 0.02f;
		}
		if (TwoStroke_UiController.instance.isModelDetected) {
			TwoStroke_UiController.instance.homeBtn.SetActive (true);
		}
	}



}
