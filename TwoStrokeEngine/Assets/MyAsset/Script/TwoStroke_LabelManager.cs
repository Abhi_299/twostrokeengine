﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStroke_LabelManager : MonoBehaviour {

	public GameObject engineInnerPart;
	public List<LabelsStore> labelObject;
}
[System.Serializable]
public class LabelsStore{

	public string labelName, labelInstruction;
	public GameObject labelObject,labelPanel;
}

